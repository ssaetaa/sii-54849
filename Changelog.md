# Changelog
Cambios funcionales realizados en el proyecto

## [1.4] 07-12-2021
### Añadido
- Programa servidor y programa cliente para el movimiento y la retransmisión del juego

### Quitado
- El juego no termina al llegar a 5 puntos

## [1.3] 18-11-2021
### Añadido
- Programa logger que muestra por pantalla cuándo un jugador marca un punto y los puntos que lleva
- Programa bot que controla la raqueta izquierda de forma automática
- Fin del juego cuando uno de los jugadores llega a 5 puntos

## [1.2] 28-10-2021
### Añadido
- Movimiento a las raquetas
- Movimiento a la esfera
- Cambio de tamaño de la esfera con el tiempo

## [1.1] 07-10-2021
### Añadido
- Archivo Changelog
- Archivo README
- Nombre del autor y número de matrícula en los ficheros de código
