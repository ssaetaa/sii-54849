// Esfera.cpp: implementation of the Esfera class.
//Silvia Saeta 54849
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
    centro.x = centro.x + velocidad.x*t;
    centro.y = centro.y + velocidad.y*t;
    
    radio = radio - 0.001;
    if (radio<0.1)
        radio = 0.5f;
    
    /*    
    velocidad.x = velocidad.x + 0.01*(velocidad.x/abs(velocidad.x));
    velocidad.y = velocidad.y + 0.01*(velocidad.y/abs(velocidad.y));
    if (abs(velocidad.x)>6){
        velocidad.x = 3;
        velocidad.y = 3;
    }
    */
}
