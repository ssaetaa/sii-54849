# Instrucciones del juego

La raqueta izquierda se puede mover con los botones W (hacia arriba) y S (hacia abajo), pero es controlada por un bot automáticamente.
La raqueta derecha se mueve con los botones O (hacia arriba) y L (hacia abajo).

Cuando la pelota toca una pared lateral vuelve a aparecer en el centro.
